## Unittesting
From within a working Netmon server installation run:
```
cd vendor/netmon-server/devices
phpunit --bootstrap ../../../bootstrap/autoload.php
```
