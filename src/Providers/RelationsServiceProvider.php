<?php

namespace Netmon\Places\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class RelationsServiceProvider extends ServiceProvider
{
    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        add_model_relation(
            'ApiServer\Users\Models\User',
            'createdCoordinates',
            function($model) {
                return $model->hasMany('Netmon\Places\Models\Coordinate', 'creator_id');
            }
        );

        add_serializer_relation(
            'ApiServer\Users\Serializers\UserSerializer',
            'createdCoordinates',
            function($serializer, $model) {
                return $serializer->hasMany($model, 'Netmon\Places\Serializers\CoordinateSerializer');
            }
        );
    }
}
