<?php

namespace Netmon\Places\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use ApiServer\Authorization\Models\Permission;
use Netmon\Places\Models\Coordinate;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        \Event::listen('api.coordinate.created', function (Coordinate $coordinate) {
            $user = \Auth::user();
            Permission::create([
                'user_id' => $user->id,
                'action_id' => 'show',
                'resource_id' => 'coordinate',
                'object_key' => $coordinate->id
            ]);
            Permission::create([
                'user_id' => $user->id,
                'action_id' => 'update',
                'resource_id' => 'coordinate',
                'object_key' => $coordinate->id
            ]);
            Permission::create([
                'user_id' => $user->id,
                'action_id' => 'destroy',
                'resource_id' => 'coordinate',
                'object_key' => $coordinate->id
            ]);
        });
    }
}
