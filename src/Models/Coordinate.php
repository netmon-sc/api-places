<?php

namespace Netmon\Places\Models;

use ApiServer\Base\Models\BaseModel;
use ApiServer\Base\Traits\UuidForKeyTrait;

class Coordinate extends BaseModel
{
    use UuidForKeyTrait;

    /**
     * Bootstrap any application services.
     */
    public static function boot()
    {
        parent::boot();

        //Register validation service
        //on saving event
        self::saving(
            function ($model) {
                return $model->validate();
            }
        );

        self::creating(
            function ($model) {
                // assign currently authenticated user as creator
                $user = \Auth::user();
                if(!empty($user->id)) {
                    $model->creator_id = $user->id;
                }
            }
        );
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'coordinates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // meta
		'creator_id',

        // general
		'latitude',
        'longitude',
        'elevation',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Define default values of model. Example:
     * @var array
     */
    protected $attributes = [];

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
        'creator_id' => 'nullable|exists:users,id',

        'latitude' => [
            'required',
            'regex:/^(\-?\d+(\.\d+)?)\.\s*(\-?\d+(\.\d+)?)$/'
        ],
        'longitude' => [
            'required',
            'regex:/^(\-?\d+(\.\d+)?)\.\s*(\-?\d+(\.\d+)?)$/'
        ],
        'elevation' => 'nullable|integer',
    ];

    /**
     * n:1 relation to users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator() {
    	return $this->belongsTo(\ApiServer\Users\Models\User::class);
    }
}
