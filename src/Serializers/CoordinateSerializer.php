<?php

namespace Netmon\Places\Serializers;

use Gate;

use ApiServer\JsonApi\Serializers\BaseSerializer;

use Netmon\Places\Models\Coordinate;

class CoordinateSerializer extends BaseSerializer
{
    protected $type = 'coordinates';

    public function getAttributes($model, array $fields = null)
    {
        if (! ($model instanceof Coordinate)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.Place::class
            );
        }

        // set up attributes
        $attributes = $model->getAttributes();
        $attributes['created_at']  = $this->formatDate($model->created_at);
        $attributes['updated_at'] = $this->formatDate($model->updated_at);

        return $attributes;
    }

    public function getLinks($model) {
        //links to always include in the resource
        $links = [
            'self' => config('app.url')."/coordinates/{$model->id}",
        ];

        //links to include based permissions
        if(Gate::allows('show', $model))
            $links['read'] = config('app.url')."/coordinates/{$model->id}";
        if(Gate::allows('update', $model))
            $links['update'] = config('app.url')."/coordinates/{$model->id}";
        if(Gate::allows('destroy', $model))
            $links['delete'] = config('app.url')."/coordinates/{$model->id}";

        return $links;
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function creator($model)
    {
        return $this->hasOne($model, \ApiServer\Users\Serializers\UserSerializer::class);
    }
}

?>
