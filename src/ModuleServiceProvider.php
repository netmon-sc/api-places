<?php

namespace Netmon\Places;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function register()
    {
        //register all the service provider this module needs
        $this->app->register(\Netmon\Places\Providers\AuthServiceProvider::class);
        $this->app->register(\Netmon\Places\Providers\EventServiceProvider::class);
        $this->app->register(\Netmon\Places\Providers\RelationsServiceProvider::class);
    }

    public function boot()
    {
        require __DIR__ . '/Http/routes.php';

        //register database migrations
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }
}

?>
