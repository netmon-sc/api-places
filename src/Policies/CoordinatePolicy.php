<?php

namespace Netmon\Places\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use ApiServer\Authorization\Policies\BasePolicy;
use Netmon\Places\Models\Coordinate;
use ApiServer\Users\Models\User;

class CoordinatePolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function index(User $authUser) {
        //everyone is allowed to list coordinates
        return true;
    }

    public function store(User $authUser, Coordinate $coordinate = null) {
        return $this->checkPermissions($authUser, 'store', 'coordinate');
    }

    public function show(User $authUser, Coordinate $coordinate) {
        //everyone is allowed to show coordinate
        return true;
    }

    public function update(User $authUser, Coordinate $coordinate) {
        return $this->checkPermissions($authUser, 'update', 'coordinate', $coordinate);
    }

    public function destroy(User $authUser, Coordinate $coordinate) {
        return $this->checkPermissions($authUser, 'destroy', 'coordinate', $coordinate);
    }
}
