<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use ApiServer\Authorization\Models\Role;
use ApiServer\Authorization\Models\Permission;
use ApiServer\Configs\Models\Config;

class CreateCoordinatePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $userRoleId = Config::where(
            'key',
            'serverUserRoleId'
        )->firstOrFail()->value;

        Permission::create([
            'role_id' => $userRoleId,
            'action_id' => 'store',
            'resource_id' => 'coordinate',
        ]);

        //admin permissions
        $adminRoleId = Config::where(
            'key',
            'serverAdminRoleId'
        )->firstOrFail()->value;
        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'index',
            'resource_id' => 'coordinate',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'show',
            'resource_id' => 'coordinate',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'store',
            'resource_id' => 'coordinate',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'update',
            'resource_id' => 'coordinate',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'destroy',
            'resource_id' => 'coordinate',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('resource_id', '=', 'coordinate')->delete();
    }
}
