<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoordinates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordinates', function (Blueprint $table) {
        	//meta
            $table->uuid('id')->primary();
            $table->uuid('creator_id')->nullabe();
            $table->foreign('creator_id')
	            ->references('id')
    	        ->on('users')
        	    ->onDelete('cascade');

            //general
            $table->float('latitude', 10, 6);
            $table->float('longitude', 10, 6);
            $table->integer('elevation')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coordinates');
    }
}
