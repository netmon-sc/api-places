<?php

Route::group([
    'namespace' => '\Netmon\Places\Http\Controllers',
    'middleware' => 'cors'], function()
{
	/**
	 * Routes using JWT auth
	 */
	Route::group(['middleware' => ['auth.jwt']], function () {
        Route::resource('coordinates', CoordinatesController::class, ['only' => [
			    'index', 'show', 'store', 'update', 'destroy'
		]]);
	});
});

?>
