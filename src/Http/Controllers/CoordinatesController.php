<?php

namespace Netmon\Places\Http\Controllers;

use ApiServer\JsonApi\Http\Controllers\DefaultResourceController;

class CoordinatesController extends DefaultResourceController
{
    public function model() {
      return \Netmon\Places\Models\Coordinate::class;
    }

    public function serializer() {
        return \Netmon\Places\Serializers\CoordinateSerializer::class;
    }

    public function resource() {
        return "coordinates";
    }
}
