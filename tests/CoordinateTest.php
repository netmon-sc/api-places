<?php

namespace Netmon\Places\Tests;

use Netmon\Server\Tests\ModuleTestCase;
use Netmon\Server\Tests\UserTest;
use Netmon\Devices\Tests\DeviceTest;

class CoordinateTest extends ModuleTestCase
{
    protected function moduleServiceProvider() {
        return \Netmon\Places\ModuleServiceProvider::class;
    }

    protected function moduleServiceProviderDepencies() {
        return [];
    }

    public function getStructure() {
		return [
			'type',
			'id',
			'attributes' => [
				'latitude',
                'longitude',
                'elevation',
			]
		];
	}

    public function testUserCanCreateCoordinate() {
		$user = UserTest::createUser();

        $authToken = \JWTAuth::fromUser($user);
		$headers = ['Authorization' => "Bearer {$authToken}"];

		$this->json(
			'POST',
			'/coordinates',
			[
				'data' => [
					'type' => "coordinate",
					'attributes' => [
						'latitude' => "52.078654",
						'longitude' => "8.654765",
                        'elevation' => "78"
					]
				]
			],
			$headers
		);
		$this->assertResponseStatus(201);
		$this->seeJsonStructure($this->getResourceStrucure());
    }
}
?>
